package de.paxii.clarinet.module.external;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;
import de.paxii.clarinet.util.chat.Chat;

/**
 * Created by Lars on 09.01.2017.
 */
public class ModuleZclip extends Module {
  public ModuleZclip() {
    super("Zclip", ModuleCategory.MOVEMENT);

    this.setDisplayedInGui(false);
    this.setCommand(true);
    this.setVersion("1.0");
    this.setBuildVersion(15801);
    this.setDescription("Teleports you on the Z axis");
    this.setSyntax("zclip <height>");
  }

  @Override
  public void onCommand(String[] args) {
    if (args.length > 0) {
      int distance = Integer.parseInt(args[0]);

      if (distance != 0) {
        if (distance >= -8 && distance <= 8) {
          double newZ = Wrapper.getPlayer().posZ + distance;

          Wrapper.getPlayer().setPosition(
                  Wrapper.getPlayer().posX,
                  Wrapper.getPlayer().posY,
                  newZ
          );

          Chat.printClientMessage(String.format("Teleported to Z: %f", newZ));
        } else {
          Chat.printClientMessage("You can't Zclip more than 8 Blocks!");
        }
      } else {
        Chat.printClientMessage("You can't Zclip to your current Z Level!");
      }
    }
  }
}
