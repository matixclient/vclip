package de.paxii.clarinet.module.external;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;
import de.paxii.clarinet.util.chat.Chat;

/**
 * Created by Lars on 09.01.2017.
 */
public class ModuleXclip extends Module {
  public ModuleXclip() {
    super("Xclip", ModuleCategory.MOVEMENT);

    this.setDisplayedInGui(false);
    this.setCommand(true);
    this.setVersion("1.0");
    this.setBuildVersion(15801);
    this.setDescription("Teleports you on the X axis");
    this.setSyntax("xclip <height>");
  }

  @Override
  public void onCommand(String[] args) {
    if (args.length > 0) {
      int distance = Integer.parseInt(args[0]);

      if (distance != 0) {
        if (distance >= -8 && distance <= 8) {
          double newX = Wrapper.getPlayer().posX + distance;

          Wrapper.getPlayer().setPosition(
                  newX,
                  Wrapper.getPlayer().posY,
                  Wrapper.getPlayer().posZ
          );

          Chat.printClientMessage(String.format("Teleported to X: %f", newX));
        } else {
          Chat.printClientMessage("You can't Xclip more than 8 Blocks!");
        }
      } else {
        Chat.printClientMessage("You can't Xclip to your current X Level!");
      }
    }
  }
}
