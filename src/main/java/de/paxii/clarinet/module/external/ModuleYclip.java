package de.paxii.clarinet.module.external;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;
import de.paxii.clarinet.util.chat.Chat;

/**
 * Created by Lars on 09.01.2017.
 */
public class ModuleYclip extends Module {
  public ModuleYclip() {
    super("Yclip", ModuleCategory.MOVEMENT);

    this.setDisplayedInGui(false);
    this.setCommand(true);
    this.setVersion("1.0");
    this.setBuildVersion(15801);
    this.setDescription("Teleports you on the Y axis");
    this.setSyntax("yclip <height>");
  }

  @Override
  public void onCommand(String[] args) {
    if (args.length > 0) {
      int distance = Integer.parseInt(args[0]);

      if (distance != 0) {
        if (distance >= -8 && distance <= 8) {
          double newY = Wrapper.getPlayer().posY + distance;

          Wrapper.getPlayer().setPosition(
                  Wrapper.getPlayer().posX,
                  newY,
                  Wrapper.getPlayer().posZ
          );

          Chat.printClientMessage(String.format("Teleported to Y: %f", newY));
        } else {
          Chat.printClientMessage("You can't Vclip more than 8 Blocks!");
        }
      } else {
        Chat.printClientMessage("You can't Vclip to your current Y Level!");
      }
    }
  }
}
