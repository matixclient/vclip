package de.paxii.clarinet.module.external;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;
import de.paxii.clarinet.util.chat.Chat;

/**
 * Created by Lars on 19.03.2016.
 */
public class ModuleVclip extends Module {
  public ModuleVclip() {
    super("Vclip", ModuleCategory.MOVEMENT);

    this.setDisplayedInGui(false);
    this.setCommand(true);
    this.setVersion("1.1");
    this.setBuildVersion(15802);
    this.setDescription("Teleports you on the Y axis");
    this.setSyntax("vclip <height>");
  }

  @Override
  public void onCommand(String[] args) {
    if (args.length > 0) {
      int height = Integer.parseInt(args[0]);

      if (height != 0) {
        if (height >= -8 && height <= 8) {
          double newY = Wrapper.getPlayer().posY + height;

          Wrapper.getPlayer().setPosition(
                  Wrapper.getPlayer().posX,
                  newY,
                  Wrapper.getPlayer().posZ
          );

          Chat.printClientMessage(String.format("Teleported to Y: %f", newY));
        } else {
          Chat.printClientMessage("You can't Vclip more than 8 Blocks!");
        }
      } else {
        Chat.printClientMessage("You can't Vclip to your current Y Level!");
      }
    }
  }
}
